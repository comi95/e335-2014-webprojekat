﻿using Microsoft.Owin;
using Owin;
using WebApp.Context;

[assembly: OwinStartupAttribute(typeof(WebApp.Startup))]
namespace WebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            using (var db = new ApplicationDataContext())
            {
                db.AppUsers.Add(new Models.AppUser{ FirstName = "Katarina"});
                db.SaveChanges();
            }
        }
    }
}

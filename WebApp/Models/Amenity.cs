﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class Amenity
    {
        [Key]
        public Guid Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
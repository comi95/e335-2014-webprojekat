﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class Comment
    {
        [Key]
        public Guid Id { get; set; }
        public User Guest { get; set; }
        public Apartment Apartment { get; set; }
        public string Text { get; set; }
        public float Review { get; set; }
    }
}
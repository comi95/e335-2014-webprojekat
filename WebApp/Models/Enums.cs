﻿namespace WebApp.Models
{
    public class Enums
    {
        public enum Genders { Male, Female };
        public enum Roles { Guest, Host, Admin };
        public enum ApartmentType { Entire, Room };
        public enum ReservationStatus { Created, Declined, Canceled, Accepted, Finished };
    }
}
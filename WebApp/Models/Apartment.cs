﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class Apartment
    {
        [Key]
        public Guid Id { get; set; }
        public Enums.ApartmentType Type { get; set; }
        public short NumberOfRooms { get; set; }
        public short NumberOfGuests { get; set; }
        public Location Location { get; set; }
        public List<DateTime> RentingDates { get; set; }
        public List<DateTime> AvailableDates { get; set; }
        public User Host { get; set; }
        public List<Comment> Comments { get; set; }
        public List<string> Photos { get; set; }
        public double PricePerNight { get; set; }
        public string CheckInTime { get; set; }
        public string CheckOutTime { get; set; }
        public bool Active { get; set; }
        public List<Amenity> Amenities { get; set; }
        public List<Reservation> Reservations { get; set; }


    }
}
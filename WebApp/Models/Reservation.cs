﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class Reservation
    {
        [Key]
        public Guid Id { get; set; }
        public Apartment Apartment { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public short NumberOfNights { get; set; }
        public double TotalPrice { get; set; }
        public User Guest { get; set; }
        public Enums.ReservationStatus Status { get; set; }
    }
}
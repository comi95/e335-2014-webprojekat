﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class Location
    {
        [Key]
        public Guid Id { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public Address Address { get; set; }
    }
}
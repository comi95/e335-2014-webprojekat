﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class Address
    {
        [Key]
        public Guid Id { get; set; }
        public string Street { get; set; }
        public int Number { get; set; }
        public string Place { get; set; }
        public int PostalNumber { get; set; }
    }
}
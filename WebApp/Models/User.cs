﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    public class User
    {
        [Key]
        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Enums.Genders Gender { get; set; }
        public Enums.Roles Role { get; set; }
        public List<Apartment> Apartments{ get; set; }
        public List<Apartment> RentedApartments { get; set; }
        public List<Reservation> Reservations { get; set; }
    }
}